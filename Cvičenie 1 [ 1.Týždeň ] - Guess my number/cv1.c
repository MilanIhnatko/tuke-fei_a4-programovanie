#include <stdio.h>
#include <stdlib.h>
#include <time.h>
 

#define MAX_POKUSY 5
#define MIN_CISLO  1
#define MAX_CISLO  50

int main()
{
	
	char restart = 'y';
	while(restart == 'y' || restart == 'Y')
	{
		srand(time(NULL));
		int uCislo = 0;
		int uPokusy = 0;
		int genCislo = (rand() % MAX_CISLO) + MIN_CISLO;

		printf("\nVyberte číslo od %d do %d.\n\n", MIN_CISLO, MAX_CISLO);

		do {
			uPokusy++;
			if(uPokusy <= MAX_POKUSY)
			{

				printf("Váš tip[Pokus č.%d]: ", uPokusy);
				scanf("%d",&uCislo );	
			
				if(uCislo > genCislo){
					printf("Hmmm..moje číslo je menšie.\n\n");
				}else if(uCislo < genCislo){
					printf("Hmmm..moje číslo je väčšie.\n\n");
				}else{
					printf("Gratulujem! Uhádol si moje číslo !!\n\n");
				}
					
			}else
				printf("Koniec hry. Moje myslené číslo je: %d\n\n", genCislo);
			
		}while((uCislo != genCislo) && uPokusy <= MAX_POKUSY);
	
		printf("Prajete si zopakovať hru ? [y/n]: ");
		scanf(" %c", &restart);

	}

	return 0;
}
